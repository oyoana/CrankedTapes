Cranked Tapes is a bicycle module, for cassette tapes to be recorded and played through the movement of the bike crank, exploring the interrelation of mechanical, organic, social rhythms and noise in the comprehension of everyday life. 

A hybrid of the famous boombox (a portable music player) and not less known bicycle ( a man powered vehicle of two wheels).  An interesting pair of urban freedom, you may call it. The bike has a long tradition of being used as an instrument (from Zappa to Ono) and the boombox has been a symbol of urban culture until the introduction of the walkman and the decline of the' loud city'. So inspired by those histories we built a bike module that allows you to play and record audio with the speed and dynamics of your biking. Turning your bike into an instrument that includes you and the environment as an active participant in listening to music in public space.  Let's make cities loud again!

more info here: http://oyoana.com/crankedtapes

The module consists of a small 555 timer circuit and a dc motor (acting as a dynamo) harvested from old printers. 